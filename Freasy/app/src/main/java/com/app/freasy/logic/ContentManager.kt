package com.app.freasy.logic

import android.util.LruCache
import com.app.freasy.GlobalApp
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.models.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList


class ContentManager {

    val maxProducts = 10

    var productsPerDeviceCache: LruCache<String, List<Product>>  = object : LruCache<String,  List<Product>>(maxProducts) {

        override fun sizeOf(key: String, bitmap:  List<Product>): Int {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return 1
        }
    }

    val maxGroups = 10

    var groupsPerProductCache: LruCache<String, List<Group>>  = object : LruCache<String, List<Group>>(maxGroups) {

        override fun sizeOf(key: String, bitmap: List<Group>): Int {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return 1
        }
    }

    val maxDevices = 50

    var deviceCache: LruCache<String, Device>  = object : LruCache<String, Device>(maxDevices) {

        override fun sizeOf(key: String, bitmap: Device): Int {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return 1
        }
    }


    var lastAddedProductCache: Product? = null

    var db = DBAdapter()



   suspend fun listenForProducts(deviceKey: String ): List<Product> = withContext(Dispatchers.IO){

            val dbResult = db.listenForProducts(deviceKey)
            if (dbResult == null){
                val snapshot: List<Product>? = productsPerDeviceCache.get(deviceKey)
                if (snapshot != null) {
                    preferences.writeCache()
                    return@withContext snapshot

                }else {
                    return@withContext listOf<Product>()
                }

            }else {
                preferences.writeCache()

                productsPerDeviceCache.put(deviceKey, dbResult)
                return@withContext dbResult
            }
    }

    suspend fun listenForDevices(): List<Device> = withContext(Dispatchers.IO){

        val dbResult = db.listenForDevices()

        if (dbResult == null){
            val snapshot: Map<String, Device> = deviceCache.snapshot()
            if (snapshot.isEmpty()){

                return@withContext listOf<Device>()
            }
            else {
                preferences.writeCache()
                return@withContext snapshot.values.toList()
            }
        }else {
            for (id in dbResult) {
                deviceCache.put(id.key, id)
            }
            preferences.writeCache()
            return@withContext dbResult

        }
    }

    suspend fun listenForAllGroups(device: String): List<Group> = withContext(Dispatchers.IO){
        val fp = listenForProducts(device)

        var result = listOf<Group>()

        for (p in fp){
            val r = listenForGroups(device, p.key!!)!!
            result = result.plus(r.asIterable())
        }

        preferences.writeCache()
        return@withContext result
    }

    suspend fun listenForGroups(device: String, productKey: String): List<Group>? = withContext(Dispatchers.IO){

        val it = db.listenForGroups(device, productKey)
        if (it == null){
            val snapshot: List<Group>? = groupsPerProductCache.get(productKey)
            if (snapshot != null){
                if (snapshot.isEmpty()){
                    return@withContext null
                }
                else {
                    preferences.writeCache()
                    return@withContext snapshot
                }
            }
            return@withContext null

        }else {
            groupsPerProductCache.put(productKey, it)
            preferences.writeCache()
            return@withContext it
        }
    }

    private suspend fun deleteGroup(product: String, group: Group ) = withContext(Dispatchers.IO){
        db.deleteGroup(group)
    }

    private suspend fun deleteProduct(product: Product ) = withContext(Dispatchers.IO){
        productsPerDeviceCache.remove(product.deviceKey)
        preferences.writeCache()

        val r = db.deleteProduct(product)
        print(r)
    }

    private suspend fun updateGroup(device:String, group: Group ) = withContext(Dispatchers.IO){
        db.updateGroup(group)
    }

    suspend fun consumeFromGroup(device: String, group: Group ) = withContext(Dispatchers.IO){
        val gg = group
        gg.remainingUnits = group.remainingUnits?.minus(1)

        if (gg.remainingUnits!! > 0) {
            updateGroup(device, gg)
        }
        if(gg.remainingUnits!! <= 0) {
            val pk = gg.productKey
            deleteGroup(device, gg)
            gg.productKey = pk
        }

        var t = 0
        for (p in listenForGroups(device, gg.productKey!!)!!){
            t += p.remainingUnits!!
        }
        if (t <= 0){
            deleteProduct(Product(key = gg.productKey))
        }
    }

    suspend fun addGroup(group: Group): Group? = withContext(Dispatchers.IO) {
        preferences.writeCache()

        return@withContext db.addGroup(group)
    }

    suspend fun addProduct(product: Product) : Product? = withContext(Dispatchers.IO){

        val p = db.addProduct(product, FirebaseAuth.getInstance().currentUser!!.email!!)
        if ( p != null) {
            lastAddedProductCache = p
            preferences.saveProduct(p)
        }
        preferences.writeCache()

        return@withContext p
    }

    public suspend fun addDevice(device: Device): Device? = withContext(Dispatchers.IO){
        preferences.writeCache()

        return@withContext db.addDevice(device, FirebaseAuth.getInstance().currentUser?.email!!)

    }

    fun addUser(user: User, onResult: (Boolean) -> Unit) {
        preferences.writeCache()

        preferences.saveUserEmail(user.email!!)
        db.addUser(user, onResult)

    }

    suspend fun listenForARecommendation(): Product?  = withContext(Dispatchers.IO) {
        if (lastAddedProductCache == null){
            lastAddedProductCache = preferences.lastProduct
        }
        return@withContext lastAddedProductCache
    }

    fun cleanCache() {

        productsPerDeviceCache = object : LruCache<String, List<Product>>(maxProducts) {

            override fun sizeOf(key: String, bitmap: List<Product>): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return 1
            }
        }


        groupsPerProductCache = object : LruCache<String, List<Group>>(maxGroups) {

            override fun sizeOf(key: String, bitmap: List<Group>): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return 1
            }
        }

        deviceCache = object : LruCache<String, Device>(maxDevices) {

            override fun sizeOf(key: String, bitmap: Device): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return 1
            }
        }

        lastAddedProductCache = null
        preferences.writeCache()

    }
}