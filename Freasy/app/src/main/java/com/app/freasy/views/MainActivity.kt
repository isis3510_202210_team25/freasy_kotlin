package com.app.freasy.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.freasy.R
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        checkForACurrentUser()
    }

    private fun showHome(email: String){
        val homeIntent = Intent(this, DeviceList::class.java)

        startActivity(homeIntent)
    }

    private fun checkForACurrentUser() {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null){
            currentUser.email?.let { showHome(it) }
        } else {
            val homeIntent = Intent(this, LogInActivity::class.java)
            startActivity(homeIntent)
        }
    }
}