package com.app.freasy.views

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.app.freasy.*
import com.app.freasy.models.Group
import com.app.freasy.models.Product
import com.app.freasy.util.DatePickerFragment
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import com.app.freasy.GlobalApp.Companion.cManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AddProduct : AppCompatActivity() {

    private lateinit var activityResultLauncher: ActivityResultLauncher<Intent>
    private lateinit var recommendationTextView:TextView

    private lateinit var editName: EditText
    private lateinit var editCalories: EditText
    private lateinit var editDescription: EditText
    private lateinit var editQuantity: EditText
    private lateinit var editDate: EditText
    private lateinit var editExpDate: EditText

    private var haveRecomendation = false
    private var recomendation: Product? = null

    private lateinit var deviceKey: String

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_product)

        val extras = intent.extras
        if (extras != null) {
            deviceKey = extras.getString("deviceKey").toString()
        }

        val voiceButton: Button = findViewById(R.id.btn_button)
        editQuantity = findViewById(R.id.brandText)
        editDate = findViewById(R.id.date_bought)
        editExpDate = findViewById(R.id.date_expires)
        val submitButton: Button = findViewById(R.id.addButton)
        val backButton: ImageButton = findViewById(R.id.back_button)

        //editDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
        //editDate.setTextIsSelectable(true);

        recommendationTextView = findViewById(R.id.recomendetionsText)
        editName = findViewById(R.id.deviceText)
        editCalories = findViewById(R.id.txtCalories)
        editDescription = findViewById(R.id.txtDescription)

        recommendationTextView.setOnClickListener{
            followTheRecomendation()
        }

        voiceButton.setOnClickListener(View.OnClickListener { val intent = Intent (RecognizerIntent. ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,  "Say the name of the product")
            try {
                activityResultLauncher.launch(intent)
            }catch(exp: ActivityNotFoundException){
                Toast.makeText(applicationContext,  "We have not detected microphone", Toast.LENGTH_SHORT).show()
            }
        })

        activityResultLauncher=registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result: ActivityResult ->
            if(result.resultCode== RESULT_OK && result.data!=null){
                val speechtext=result.data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                editName.setText(speechtext!![0], TextView.BufferType.EDITABLE)
            }
        }

        backButton.setOnClickListener {
            val intent = Intent(this, DeviceList::class.java)
            startActivity(intent)
        }

        submitButton.setOnClickListener{
            add()
        }

        editDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDatePickerDialog(editDate)
            }
        }

        editDate.setOnClickListener {
            showDatePickerDialog(editDate)
        }

        editExpDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDatePickerDialog(editExpDate)
            }
        }

        editExpDate.setOnClickListener {
            showDatePickerDialog(editExpDate)
        }

        listenForARecommendation()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun add(){
        if(verifyFields(editName, editQuantity, editDate, editExpDate)) {
            val product = Product("",
                deviceKey, editCalories.text.toString().toInt(), editName.text.toString(), editDescription.text.toString(), "")
            val group = Group("", "", editDate.text.toString(), editExpDate.text.toString(), editQuantity.text.toString().toInt())

            CoroutineScope(Dispatchers.Main).launch {
                val p = cManager.addProduct(product)
                if(p != null){
                    group.productKey = p.key
                    val g = cManager.addGroup(group)
                    if(g!=null){
                        goBack()
                    } else {
                        showAlert("Add product", "Process was completed unsuccessfully")
                    }

                } else {
                    showAlert("Add product", "Process was completed unsuccessfully")
                }
            }
        }
    }

    private fun goBack(){
        val homeIntent = Intent(this, DeviceList::class.java)
        startActivity(homeIntent)
    }

    private fun listenForARecommendation() {
        CoroutineScope(Dispatchers.Main).launch {
            recomendation = cManager.listenForARecommendation()
            if(recomendation != null){
                haveRecomendation = true
                recommendationTextView.text = "Freasy recommendation: click here to add more ${recomendation!!.name} to your fridge!!"
            }
        }
    }

    private fun followTheRecomendation(){
        if(haveRecomendation && recomendation != null) {
            haveRecomendation = false
            editName.setText(recomendation!!.name, TextView.BufferType.EDITABLE)
            editCalories.setText("${recomendation!!.calories!!}", TextView.BufferType.EDITABLE)
            editDescription.setText(recomendation!!.description, TextView.BufferType.EDITABLE)
            recommendationTextView.text = "Nice!! We have fill the data for your recomendation. Just fill the rest of the product info, or click here to reset the fields."
        } else {
            haveRecomendation = false
            editName.setText("", TextView.BufferType.EDITABLE)
            editCalories.setText("", TextView.BufferType.EDITABLE)
            editDescription.setText("", TextView.BufferType.EDITABLE)
            recommendationTextView.text = ""
        }
    }

    private fun showDatePickerDialog(text: EditText) {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year, text) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int, text: EditText) {
        val monthI = month + 1
        var dayS = ""+day
        if(day < 10){
            dayS = "0$dayS"
        }
        var monthS = ""+monthI
        if(month < 10){
            monthS = "0$monthS"
        }
        text.setText("$dayS/$monthS/$year")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun verifyFields(name: EditText, quantity: EditText, shopDate: EditText, exDate: EditText): Boolean {
        if (name.text.toString().isEmpty()) {
            name.error = "Please enter a name to continue"
            name.requestFocus()
            return false
        }

        try {
            val parsedInt = quantity.text.toString().toInt()

            if (parsedInt<1) {
                name.error = "Please enter a valid quantity to continue"
                name.requestFocus()
                return false
            }

        } catch (nfe: NumberFormatException) {
            quantity.error = "Please enter a valid quantity to continue"
            quantity.requestFocus()
            return false
        }

        if (shopDate.text.toString().isEmpty()) {
            shopDate.error = "Please choose a date"
            shopDate.requestFocus()
            return false
        }

        if (exDate.text.toString().isEmpty()) {
            exDate.error = "Please choose a date"
            exDate.requestFocus()
            return false
        }

        val shopDateValue =
            LocalDate.parse(shopDate.text.toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDay =
            LocalDate.parse(sdf.format(Date()), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

      //  if(shopDateValue.dayOfYear < currentDay.dayOfYear || shopDateValue.year < currentDay.year){
//
      //      val d = shopDateValue.dayOfYear
      //      val e =  currentDay.dayOfYear
      //      Toast.makeText(this, "Please choose a valid shopping date $d $e", Toast.LENGTH_SHORT).show()
      //      shopDate.requestFocus()
      //      return false
      //  }


        val exDateValue =
            LocalDate.parse(exDate.text.toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

      // if(shopDateValue.dayOfYear > exDateValue.dayOfYear || shopDateValue.year > exDateValue.year || exDateValue.dayOfYear < currentDay.dayOfYear || exDateValue.year < currentDay.year){

      //     Toast.makeText(this, "Please choose a valid expiration date", Toast.LENGTH_SHORT).show()
      //     exDate.requestFocus()
      //     return false
      // }
        return true
    }

    private fun showAlert(tittle: String, alert: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(tittle)
        builder.setMessage(alert)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}

