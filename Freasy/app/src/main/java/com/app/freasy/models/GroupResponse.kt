package com.app.freasy.models

import com.google.gson.annotations.SerializedName

class GroupResponse {
    @SerializedName("data") var data: List<Group>? = null
}