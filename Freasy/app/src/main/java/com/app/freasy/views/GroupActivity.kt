package com.app.freasy.views

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.GlobalApp
import com.app.freasy.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.app.freasy.models.Group
import com.app.freasy.util.GroupListAdapter

class GroupActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var placeHolder: TextView
    private var groups = listOf<Group>()

    private lateinit var key: String
    private lateinit var name: String
    private lateinit var device: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.default_list)

        placeHolder = findViewById(R.id.placeHolder)

        val extras = intent.extras
        if (extras != null) {
            key = extras.getString("key").toString()
            name = extras.getString("name").toString()
            device = extras.getString("device").toString()
        }

        val backButton: ImageButton = findViewById(R.id.backButtonCup)
        backButton.setOnClickListener {
            val intent = Intent(this, DeviceList::class.java)
            startActivity(intent)
        }

        val addBut: Button = findViewById(R.id.mainButtomCup)
        addBut.setOnClickListener {
            val intent = Intent(this, AddGroupItem::class.java)

            intent.putExtra("productName", name);
            intent.putExtra("productKey", key);
            intent.putExtra("device", device);
            startActivity(intent)
        }

        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val title: TextView = findViewById(R.id.tittle)
        title.text = name

        recyclerView.layoutManager = LinearLayoutManager(this)


        CoroutineScope(Dispatchers.Main).launch {
            val groups = GlobalApp.cManager.listenForGroups(device, key)
            getProducts(groups)
        }

    }

    private fun getProducts  ( _products: List<Group>? ){
        placeHolder.text = ""
        if(_products != null) {
            groups = _products
            val adapter = GroupListAdapter(_products)
            recyclerView.adapter = adapter
            adapter.setOnClickListener(object : GroupListAdapter.OnCardListener {
                override fun onCardClick(position: Int){
                    openInfo(position)
                }
            })
        }
    }

    fun openInfo(i: Int){
        val intent = Intent(this, ItemInfoActivity::class.java)
        intent.putExtra("key", key);
        intent.putExtra("groupKey", groups[i].key);
        intent.putExtra("name", name);

        intent.putExtra("remainingUnits", groups[i].remainingUnits.toString());
        intent.putExtra("shoppingDate", groups[i].addedAt);
        intent.putExtra("expirationDate", groups[i].expiresAt);
        intent.putExtra("device", device);
        intent.putExtra("productCurrentUnits", groups[i].remainingUnits);
        startActivity(intent)
    }


}