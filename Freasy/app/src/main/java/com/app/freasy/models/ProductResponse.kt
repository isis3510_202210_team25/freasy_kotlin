package com.app.freasy.models

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("data") var data: List<Product>? = null,
)
