package com.app.freasy

import android.app.Application
import com.app.freasy.logic.ContentManager
import com.app.freasy.logic.LocalContentManager

class GlobalApp: Application() {

    companion object {
        lateinit var cManager: ContentManager
        lateinit var preferences: LocalContentManager
        var user: String = ""
    }

    override fun onCreate() {
        super.onCreate()

        preferences = LocalContentManager(applicationContext)
        cManager = ContentManager()
        try {
            preferences.readCache()
        }catch (e: Exception){

        }


    }


}