package com.app.freasy.views

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.R
import com.app.freasy.models.Group
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate.parse
import java.time.format.DateTimeFormatter
import java.util.*

class ItemInfoActivity() : AppCompatActivity() {

    private lateinit var name: String
    private lateinit var key: String
    private lateinit var device: String
    private var productCurrentUnits: Int = 0


    private lateinit var groupKey: String
    private lateinit var quantity: String
    private lateinit var shoppingDate: String
    private lateinit var expirationDate: String

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cupboard_item_info)

        val extras = intent.extras
        if (extras != null) {
            key = extras.getString("key").toString()
            groupKey = extras.getString("groupKey").toString()
            name = extras.getString("name").toString()
            quantity = extras.getString("remainingUnits").toString()
            shoppingDate = extras.getString("shoppingDate").toString()
            expirationDate = extras.getString("expirationDate").toString()
            device = extras.getString("device").toString()
            productCurrentUnits = extras.getInt("productCurrentUnits")
        }

        val backButton: ImageButton = findViewById(R.id.backButtonCup)
        backButton.setOnClickListener {
            val intent = Intent(this, GroupActivity::class.java)
            intent.putExtra("key", key);
            intent.putExtra("name", name);
            intent.putExtra("device", device);
            startActivity(intent)
        }

        val consumeBut: Button = findViewById(R.id.mainButtomCup)
        consumeBut.setOnClickListener {

            CoroutineScope(Dispatchers.Main).launch {
                val d = Group(productKey = key, groupKey, shoppingDate, expirationDate, quantity.toInt())
                cManager.consumeFromGroup(device, d)
                goBack()
            }


        }

        var stats: TextView = findViewById(R.id.placeHolder)
        var shoppingDateView: TextView = findViewById(R.id.shoppingDate)
        var expirationDateView: TextView = findViewById(R.id.expirationDate)
        var productNameView: TextView = findViewById(R.id.productName)
        var quantityView: TextView = findViewById(R.id.quantity)

        shoppingDateView.text = shoppingDate
        expirationDateView.text = expirationDate
        productNameView.text = name
        quantityView.text = "Stock: $quantity"

        try {
            val startDateValue = parse(shoppingDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"))

            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val endDateValue = parse(sdf.format(Date()), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

            val diff = endDateValue.dayOfYear - startDateValue.dayOfYear

            stats.text = "This item have been on your cupboard for " + diff + " days."
        } catch (e: Exception){}
    }

    fun goBack(){
        val intent = Intent(this, DeviceList::class.java)
        intent.putExtra("key", key);
        intent.putExtra("name", name);
        intent.putExtra("device", device);
        startActivity(intent)
    }

}
