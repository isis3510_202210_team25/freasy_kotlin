package com.app.freasy.views

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.app.freasy.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import android.content.SharedPreferences
import android.widget.CheckBox
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.GlobalApp.Companion.preferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class LogInActivity : AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    lateinit var sharedPreferences: SharedPreferences
    lateinit var rememberMe: CheckBox
    lateinit var emailTxt: TextView
    lateinit var passwordTxt: TextView

    var isRemembered=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loginl)

        val firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Login")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle)

        sharedPreferences = getSharedPreferences( "SHARED_PREF", Context.MODE_PRIVATE)
        isRemembered = sharedPreferences.getBoolean("CHECKBOX", false)

        setup()
    }

    private fun setup() {
        title = "Authentication"
        var signUpButton: TextView = findViewById(R.id.logInButton)
        var logInButton: Button = findViewById(R.id.signUpButton)
        emailTxt = findViewById(R.id.txtEmail)
        passwordTxt = findViewById(R.id.txtPassword)
        rememberMe = findViewById(R.id.remember_me)

        if (isRemembered){
            val email = sharedPreferences.getString("EMAIL","")
            emailTxt.text = email
            val password = sharedPreferences.getString("PASSWORD","")
            passwordTxt.text = password
        }

        signUpButton.setOnClickListener{
            val intent = Intent(this, SingUpActivity::class.java)
            startActivity(intent)
        }
        logInButton.setOnClickListener {
            logIn()
        }
    }

    private fun logIn() {
        val checked: Boolean = rememberMe.isChecked

        if (verifyFields(emailTxt, passwordTxt)){

            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("EMAIL", emailTxt.text.toString())
            editor.putString("PASSWORD", passwordTxt.text.toString())
            editor.putBoolean("CHECKBOX", checked)
            editor.apply()

            FirebaseAuth.getInstance().signInWithEmailAndPassword(emailTxt.text.toString(), passwordTxt.text.toString()).addOnCompleteListener {
                if (it.isSuccessful){
                    cManager.db.email = emailTxt.text.toString()
                    preferences.saveUserEmail(emailTxt.text.toString())
                    CoroutineScope(Dispatchers.Main).launch{
                        cManager.listenForDevices()
                        showHome(it.result?.user?.email ?: "")

                    }
                } else{
                    showAlert("Log In", "The email/password don't match or the user doesn't exist.")
                }
            }
        }
    }

    private fun showHome(email: String){
        val homeIntent = Intent(this, HomeActivity::class.java).apply {
            putExtra("email", email)
        }
        startActivity(homeIntent)
    }

    private fun verifyFields(email: TextView, password: TextView): Boolean {
        if (email.text.toString().isEmpty()) {
            email.error = "Please enter your email to continue"
            email.requestFocus()
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            email.error = "Please enter a valid email to continue"
            email.requestFocus()
            return false
        }

        if (password.text.toString().isEmpty()) {
            password.error = "Please enter your password to continue"
            password.requestFocus()
            return false
        }

        return true
    }

    private fun showAlert(tittle: String, alert: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(tittle)
        builder.setMessage(alert)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
