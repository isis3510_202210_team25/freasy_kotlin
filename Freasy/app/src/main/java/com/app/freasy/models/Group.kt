package com.app.freasy.models

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class Group(
    @SerializedName("productKey") var productKey: String? = "",
    @SerializedName("key") var key: String? = "",
    @SerializedName("addedAt") var addedAt: String? = "",
    @SerializedName("expiresAt") var expiresAt: String? = "",
    @SerializedName("remainingUnits") var remainingUnits: Int? = 0
)
