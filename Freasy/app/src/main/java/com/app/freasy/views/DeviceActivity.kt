package com.app.freasy.views

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.models.Product
import com.app.freasy.util.ProductListAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

import  com.app.freasy.GlobalApp.Companion.cManager

class DeviceActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var placeHolder: TextView
    private var products = listOf<Product>()

    private lateinit var name: String
    private lateinit var key: String


    override fun onResume() {
        super.onResume()

        val extras = intent.extras
        if (extras != null) {
            key = extras.getString("deviceKey").toString()
            name = extras.getString("deviceName").toString()
        }

        CoroutineScope(Dispatchers.Main).launch {
            val products = cManager.listenForProducts(key)
            getProducts(products)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.default_list)
        val bundle: Bundle? = intent.extras

        val extras = intent.extras
        if (extras != null) {
            key = extras.getString("deviceKey").toString()
            name = extras.getString("deviceName").toString()
        }

        placeHolder = findViewById(R.id.placeHolder)

        val backButton: ImageButton = findViewById(R.id.backButtonCup)
        backButton.setOnClickListener {
            val intent = Intent(this, DeviceList::class.java)
            startActivity(intent)
        }

        val addBut: Button = findViewById(R.id.mainButtomCup)
        addBut.setOnClickListener {
            val intent = Intent(this, AddProduct::class.java)
            intent.putExtra("deviceKey", key);

            startActivity(intent)
        }
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val title: TextView = findViewById(R.id.tittle)
        title.text = name

        recyclerView.layoutManager = LinearLayoutManager(this)


    }

    private fun getProducts  ( _products: List<Product>? ){
        placeHolder.text = ""
        if(_products != null) {
            products = _products
            val adapter = ProductListAdapter(_products)
            recyclerView.adapter = adapter
            adapter.setOnClickListener(object : ProductListAdapter.OnCardListener {
                override fun onCardClick(position: Int){
                    openGroup(position)
                }
            })
        }
    }


    fun openGroup(i: Int){
        val intent = Intent(this, GroupActivity::class.java)
        intent.putExtra("key", products[i].key);
        intent.putExtra("device", key);
        intent.putExtra("name", products[i].name);

        startActivity(intent)
    }


}