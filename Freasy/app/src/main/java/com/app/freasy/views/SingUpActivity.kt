package com.app.freasy.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.app.freasy.GlobalApp
import com.app.freasy.R
import com.app.freasy.models.User
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import  com.app.freasy.GlobalApp.Companion.cManager


class SingUpActivity : AppCompatActivity() {

    lateinit var emailTxt: TextView
    lateinit var  passwordTxt: TextView
    lateinit var  nameTxt: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singup)

        val firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Sign up")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
        setup()
    }


    private fun setup() {
        title = "Authentication"
        val signUpButton: Button = findViewById(R.id.signUpButton)
        val logInButton: TextView = findViewById(R.id.logInButton)
        emailTxt = findViewById(R.id.txtEmail)
        passwordTxt = findViewById(R.id.txtPassword)
        nameTxt = findViewById(R.id.txtNameP)
        var a = false
        var b = false

        signUpButton.setOnClickListener {
            singUp()
        }
        logInButton.setOnClickListener {

            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    private fun singUp(){
        if (verifyFields(emailTxt, passwordTxt)) {
            CoroutineScope(Dispatchers.Main).launch {
                val email = emailTxt.text.toString()
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    emailTxt.text.toString(),
                    passwordTxt.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = User(
                            email,
                            "",
                            nameTxt.text.toString(),
                            emailTxt.text.toString(),
                            passwordTxt.text.toString()
                        )
                        cManager.addUser(user, fun(s: Boolean) {
                            if (s) {
                                cManager.db.email = emailTxt.text.toString()
                                GlobalApp.preferences.saveUserEmail(emailTxt.text.toString())
                                showHome(user.email!!)
                            } else {
                                showAlert(
                                    "Sing Up",
                                    "There's already a user with the same email."
                                )
                            }
                        })
                    } else {
                        showAlert("Sing Up", "${it.exception}")
                        showAlert("Sing Up", "There's already a user with the same email.")
                    }
                }
            }
        }
    }
    private fun showAlert(tittle: String, alert: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(tittle)
        builder.setMessage(alert)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showHome(email: String) {
        val homeIntent = Intent(this, HomeActivity::class.java).apply {
            putExtra("email", email)
        }
        startActivity(homeIntent)
    }


    private fun verifyFields(email: TextView, password: TextView): Boolean {
        if (email.text.toString().isEmpty()) {
            email.error = "Please enter an email to continue"
            email.requestFocus()
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            email.error = "Please enter a valid email to continue"
            email.requestFocus()
            return false
        }

        if (password.text.toString().isEmpty()) {
            password.error = "Please enter a password to continue"
            password.requestFocus()
            return false
        }

        if (password.text.toString().count() <= 5) {
            password.error = "The password should be at least 6 characters long"
            password.requestFocus()
            return false
        }

        return true
    }
}