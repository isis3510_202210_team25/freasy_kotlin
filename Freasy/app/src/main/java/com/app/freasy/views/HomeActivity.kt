package com.app.freasy.views

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.app.freasy.R
import com.google.firebase.auth.FirebaseAuth
import  com.app.freasy.GlobalApp.Companion.cManager

class HomeActivity : AppCompatActivity() {

    private lateinit var email: String
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val statisticsButton: Button = findViewById(R.id.statisticsButton)
        val devicesButton: Button = findViewById(R.id.devicesButton)
        val bundle: Bundle? = intent.extras

        email = bundle?.getString("email").toString()
        val provider = bundle?.getString("provider")
        setup(email?: "", provider?: "")

        statisticsButton.setOnClickListener {
            val intent = Intent(this, StatsActivity::class.java)
            startActivity(intent)
        }


        devicesButton.setOnClickListener {
            val intent = Intent(this, DeviceList::class.java)
            startActivity(intent)
        }

    }

    private fun setup(email: String, provider: String) {
        title = "Home"
        val logOutButton: Button = findViewById(R.id.signOutButton)

        logOutButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            cManager.cleanCache()
            val homeIntent = Intent(this, LogInActivity::class.java)
            startActivity(homeIntent)
        }
    }
}