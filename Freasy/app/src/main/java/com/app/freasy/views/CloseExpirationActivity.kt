package com.app.freasy.views

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.annotation.RequiresApi
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.R
import com.app.freasy.models.Product
import com.app.freasy.util.ProductListAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CloseExpirationActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private var products = listOf<Product>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_close_expiration)

        val backButton: ImageButton = findViewById(R.id.backButtonCupC)
        backButton.setOnClickListener {
            val intent = Intent(this, StatsActivity::class.java)
            startActivity(intent)
        }

        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewC)
        var title: TextView = findViewById(R.id.tittleC)
        title.text = "Items close to expiring"
        recyclerView.layoutManager = LinearLayoutManager(this)

        CoroutineScope(Dispatchers.Main).launch {

            val devices = cManager.listenForDevices()
            for (device in devices) {
                val products = cManager.listenForProducts(device.key!!)
                getProducts(products, device.key!!)
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun getProducts(_products: List<Product>?, key: String) {
        val current = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val currentDate = LocalDate.parse(current.toString(), formatter)
        var s: String
        var y: Int = 0
        var m: Int = 0
        var d: Int = 0
        var f: List<String?> = List<String?>(3){null; null; null}
        var date: LocalDate
        if (_products != null) {
            for (p in _products) {
                var groups = cManager.listenForGroups(key, p.key!!)
                for (g in groups!!) {
                    s = g.expiresAt!!
                    f = s?.split("/")
                    y = f!![2].toInt()
                    m = f!![1].toInt()
                    d = f!![0].toInt()
                    date = LocalDate.of(y, m, d)
                    var comp = date.compareTo(currentDate)
                    if (comp == 0) {
                        products += p
                        val adapter = ProductListAdapter(products)
                        recyclerView.adapter = adapter
                        adapter.setOnClickListener(object : ProductListAdapter.OnCardListener {
                            override fun onCardClick(position: Int) {
                                openGroup(position)
                            }
                        })
                    }
                }
            }
        }
    }

    fun openGroup(i: Int) {
        val intent = Intent(this, GroupActivity::class.java)
        intent.putExtra("key", products[i].key);
        intent.putExtra("device", "cupboard");

        startActivity(intent)
    }
}