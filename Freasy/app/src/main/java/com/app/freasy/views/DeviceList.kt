package com.app.freasy.views

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.models.Device
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

import  com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.util.DeviceListAdapter

class DeviceList : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var placeHolder: TextView
    private var devices = listOf<Device>()

    override fun onResume() {
        super.onResume()

        CoroutineScope(Dispatchers.Main).launch {
            val devices = cManager.listenForDevices()
            setDevices(devices)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.default_menu_list)
        val bundle: Bundle? = intent.extras

        placeHolder = findViewById(R.id.placeHolder)

        val backButton: ImageButton = findViewById(R.id.backButtonCup)
        backButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        val addBut: Button = findViewById(R.id.mainButtomCup)
        addBut.setOnClickListener {
            val intent = Intent(this, AddDevice::class.java)
            startActivity(intent)
        }
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        var title: TextView = findViewById(R.id.tittle)
        title.text = "Devices"

        recyclerView.layoutManager = LinearLayoutManager(this)


    }

    private fun setDevices  (_devices: List<Device>? ){
       placeHolder.text = ""
       if(_devices != null) {
            devices = _devices
            val adapter = DeviceListAdapter(_devices)
            recyclerView.adapter = adapter
            adapter.setOnClickListener(object : DeviceListAdapter.OnCardListener {
                override fun onCardClick(position: Int){
                    openGroup(position)
                }
            })
        }
    }


    fun openGroup(i: Int){
        val intent = Intent(this, DeviceActivity::class.java)
        intent.putExtra("deviceKey", devices[i].key);
        intent.putExtra("deviceName", devices[i].label);
        //intent.putExtra("name", devices[i].name);

        startActivity(intent)
    }


}