package com.app.freasy.views

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.models.Group
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate.parse
import java.time.format.DateTimeFormatter
import java.util.*
import  com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.models.Product
import com.app.freasy.util.textListAdapter
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@RequiresApi(Build.VERSION_CODES.O)
class StatsActivity() : AppCompatActivity() {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val currentDate = LocalDate.parse(LocalDate.now().toString(), formatter)
    private var products = listOf<Product>()
    private var count: Int = 0
    private var avg: Long = 0
    private lateinit var recyclerView: RecyclerView
    private lateinit var placeHolder: TextView


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)

        val backButton: ImageButton = findViewById(R.id.backButtonCup)
        backButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
        val closeButton: Button = findViewById(R.id.buttonCloseExpiration)
        closeButton.setOnClickListener {
            val intent = Intent(this, CloseExpirationActivity::class.java)
            startActivity(intent)
        }
        val expiredButton: Button = findViewById(R.id.expiredItemsButton)
        expiredButton.setOnClickListener {
            val intent = Intent(this, RottenItemsActivity::class.java)
            startActivity(intent)
        }

        val preferedProductsButton: Button = findViewById(R.id.buttonPreferedFoods)
        preferedProductsButton.setOnClickListener {
            val intent = Intent(this, PreferedFoods::class.java)
            startActivity(intent)
        }

        placeHolder = findViewById(R.id.placeHolder)

        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)

        setList()

    }

    private fun setList () {
        CoroutineScope(Dispatchers.Main).launch {
            val stats = arrayListOf<String>()

            var total:Long = 0
            val devices = cManager.listenForDevices()
            for (device in devices){
                val groups = cManager.listenForAllGroups(device.key!!)
                val expiredItems = calculate(groups)
                stats.add( "$expiredItems items have expired on your ${device.label}.")

                val productsF = cManager.listenForProducts(device.key!!)
                total += getProducts(productsF, device.key!!)
            }

            if (count != 0) {
                avg = (total / count)
            }
            stats.add("The average time for an item to spoil is: $avg ")
            val adapter = textListAdapter(stats)
            recyclerView.adapter = adapter

            adapter.setOnClickListener(object : textListAdapter.OnCardListener {
                override fun onCardClick(position: Int){
                }
            })
            placeHolder.text = ""

        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun calculate(expirationDateF: List<Group>): Int = withContext(Dispatchers.Default) {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val endDay = parse(sdf.format(Date()), DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        var expiredItems = 0
        for (ex in expirationDateF)
        {
            val startDateValue = parse(ex.expiresAt, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
            if (startDateValue.dayOfYear < endDay.dayOfYear || startDateValue.year < endDay.year)
            {
                expiredItems ++
            }

        }
        return@withContext expiredItems
    }


    private suspend fun getProducts(_products: List<Product>?, key: String): Long {
        var temp: Long = 0
        var s: String
        var y: Int = 0
        var m: Int = 0
        var d: Int = 0
        var f: List<String?> = List<String?>(3){null; null; null}
        var date: LocalDate
        if (_products != null) {
            for (p in _products) {
                val groups = cManager.listenForGroups(key, p.key!!)
                for (g in groups!!) {
                    s = g.expiresAt!!
                    f = s?.split("/")
                    y = f!![2].toInt()
                    m = f!![1].toInt()
                    d = f!![0].toInt()
                    date = LocalDate.of(y, m, d)
                    var comp = date.compareTo(currentDate)
                    if (comp > 0) {
                        products += p
                        println("$date")
                        println("$currentDate")
                        var resp: Long = ChronoUnit.DAYS.between(currentDate, date)
                        println("$resp")
                        temp += resp
                        count++
                    }
                }
            }
        }
        return temp
    }

}
