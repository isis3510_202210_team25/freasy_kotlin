package com.app.freasy.views

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.app.freasy.*
import java.util.*
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.models.Device
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AddDevice : AppCompatActivity() {

    private lateinit var deviceText: EditText
    private lateinit var brandText: EditText

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_device)

        deviceText = findViewById(R.id.deviceText)
        brandText = findViewById(R.id.brandText)

        val addButton: Button = findViewById(R.id.addButton)
        val backButton: ImageButton = findViewById(R.id.back_button)

        backButton.setOnClickListener {
            val intent = Intent(this, DeviceList::class.java)
            startActivity(intent)
        }

        addButton.setOnClickListener{add()}
    }

    private fun add(){
        if(verifyFields(this.deviceText, brandText)) {
            val device = Device("","", deviceText.text.toString(), brandText.text.toString())

            CoroutineScope(Dispatchers.Main).launch {
                val dev = cManager.addDevice(device)

                if (dev != null) {
                    goToDeviceList()
                }else{
                    showAlert("Add device", "Process was completed unsuccessfully")
                }
            }

        }

    }

    private fun goToDeviceList(){
        val intent = Intent(this, DeviceList::class.java)
        startActivity(intent)
    }
    private fun verifyFields(name: EditText, brand: EditText): Boolean {
        if (name.text.toString().isEmpty()) {
            name.error = "Please enter a name to continue"
            name.requestFocus()
            return false
        }

        if (brand.text.toString().isEmpty()) {
            brand.error = "Please enter a name to continue"
            brand.requestFocus()
            return false
        }
        return true
    }

    private fun showAlert(tittle: String, alert: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(tittle)
        builder.setMessage(alert)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
