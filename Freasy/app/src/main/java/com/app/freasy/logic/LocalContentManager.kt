package com.app.freasy.logic

import android.content.Context
import android.util.LruCache
import com.app.freasy.GlobalApp
import com.app.freasy.models.Device
import com.app.freasy.models.Group
import com.app.freasy.models.Product
import com.google.firebase.auth.FirebaseAuth
import java.io.*
import kotlinx.serialization.json.*
import kotlinx.serialization.*

class LocalContentManager(val context:Context) {

    private val storage = context.getSharedPreferences("preferences", 0)

    var lastProduct: Product? = null
    var email: String? = null


    init {
        email = storage.getString("userEmail", "")

        if(email != "" && email == FirebaseAuth.getInstance().currentUser?.email){

            val lastProductCal = storage.getInt("lastProduct_cal", -1)
            if(lastProductCal != -1){
                val lastProductName = storage.getString("lastProduct_name", "")!!
                val lastProductDescription = storage.getString("lastProduct_description", "")!!
                lastProduct = Product(name = lastProductName, calories = lastProductCal, description = lastProductDescription )
            }
        }

    }

    fun getUserEmail(): String?{
        return email
    }

    fun saveUserEmail(email:String){
        storage.edit().putString("userEmail", email).apply()
    }


    fun writeCache(){
        val filename = "cache"

        var contentString:String = Json.encodeToString(GlobalApp.cManager.productsPerDeviceCache.snapshot())
        File(context.filesDir, filename+"productsPerDeviceCache").printWriter().use { out -> out.println(contentString) }

        println("done $contentString")

        contentString = Json.encodeToString( GlobalApp.cManager.deviceCache.snapshot())
        File(context.filesDir, filename+"deviceCache").printWriter().use { out -> out.println(contentString) }

        println("done $contentString")

        contentString = Json.encodeToString( GlobalApp.cManager.groupsPerProductCache.snapshot())
        File(context.filesDir, filename+"groupsPerProductCache").printWriter().use { out -> out.println(contentString) }

        println("done $contentString")

    }


    fun readCache(){
        val filename = "cacheproductsPerDeviceCache"
        var result: Map<String, List<Product>>? = null
        File(context.filesDir, filename).forEachLine { result = Json.decodeFromString<Map<String, List<Product>>>(it)}

        for (d in result!!.keys){
            GlobalApp.cManager.productsPerDeviceCache.put(d, result!!.get(d))
        }

        println("result: $result")

        val filename2 = "cachedeviceCache"
        var result2: Map<String,Device>? = null
        File(context.filesDir, filename2).forEachLine { result2 = Json.decodeFromString<Map<String,Device>>(it)}

        for (d in result2!!.keys){
            GlobalApp.cManager.deviceCache.put(d, result2!!.get(d))
        }

        println("result: $result2")

        val filename3 = "cachegroupsPerProductCache"
        var result3: Map<String,List<Group>>? = null
        File(context.filesDir, filename3).forEachLine { result3 = Json.decodeFromString<Map<String,List<Group>>>(it)}

        for (d in result3!!.keys){
            GlobalApp.cManager.groupsPerProductCache.put(d, result3!!.get(d))
        }

        println("result: $result3")
    }


    fun saveProduct(key:Product){
        lastProduct = key
        storage.edit().putString("userEmail", FirebaseAuth.getInstance().currentUser?.email).apply()
        storage.edit().putInt("lastProduct_cal", key.calories!!).apply()
        storage.edit().putString("lastProduct_name", key.name!!).apply()
        storage.edit().putString("lastProduct_description", key.description!!).apply()
    }

}