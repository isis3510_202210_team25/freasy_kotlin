package com.app.freasy.logic

import com.app.freasy.models.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface RestApi {

    @Headers("Content-Type: application/json")
    @POST
    fun addUser(@Body user: User, @Url url: String): Call<User>

    @Headers("Content-Type: application/json")
    @POST
    fun addDevice(@Body device: Device, @Url url: String): Call<Device>

    @Headers("Content-Type: application/json")
    @POST
    fun addProduct(@Body product: Product, @Url url: String): Call<Product>

    @Headers("Content-Type: application/json")
    @POST
    fun addGroup(@Body product: Group, @Url url: String): Call<Group>

    @GET
    suspend fun getUserProducts(@Url url: String): Response<ProductResponse>

    @GET
    suspend fun getUserDevices(@Url url: String): Response<DeviceResponse>

    @GET
    suspend fun getUserGroups(@Url url: String): Response<GroupResponse>


    @Headers("Content-Type: application/json")
    @PUT
    fun updateGroup(@Body group: Group, @Url url: String): Call<Group>

    @Headers("Content-Type: application/json")
    @PUT
    fun updateProduct(@Body group: Product, @Url url: String): Call<Product>

}