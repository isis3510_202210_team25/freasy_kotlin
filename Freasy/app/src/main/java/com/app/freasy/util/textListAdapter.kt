package com.app.freasy.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.models.Device


class textListAdapter(var texts: List<String>): RecyclerView.Adapter<textListAdapter.item>() {

    inner class item(itemView: View, listener: OnCardListener): RecyclerView.ViewHolder(itemView) {

        var textView: TextView = itemView.findViewById(R.id.text)

        init {

            itemView.setOnClickListener{
                listener.onCardClick(adapterPosition)
            }

        }
    }

    fun clear() {
        texts = listOf<String>()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): item {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.text_item, viewGroup, false)
        return item(v, listener)
    }

    override fun onBindViewHolder(viewHolder: item, i: Int) {
        viewHolder.textView.text = texts[i]
    }

    override fun getItemCount(): Int {
        return texts.size
    }

    private lateinit var listener: OnCardListener

    fun setOnClickListener(mlistener: OnCardListener){
        listener = mlistener
    }

    public interface OnCardListener{
        fun onCardClick(position: Int)
    }
}