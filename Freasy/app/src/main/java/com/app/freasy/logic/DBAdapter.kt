package com.app.freasy.logic

import com.app.freasy.models.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList
import kotlin.reflect.KFunction1
import com.app.freasy.GlobalApp.Companion.preferences

class DBAdapter {

    var email: String = preferences.getUserEmail()!!

    //private var dataBaseReference : DatabaseReference

    //Just for development purposes
    //private val email = "GgVhiLxjlMuss64K6CoV"

    /*init {
        dataBaseReference = db.getReference(userEmail)
    }*/

    fun retrieveContainers(adapter: KFunction1<ArrayList<String>, Unit>) {
        val result = arrayListOf<String>()
        /*dataBaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    for(data in snapshot.children){
                        result.add(data.key.toString())
                    }
                    adapter(result)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })*/
    }

    /*
    private fun getProductsDBReference(deviceLabel: String): DatabaseReference {
        return db.getReference("UsersProducts/$userEmail/$deviceLabel")

    }

    private fun getProductDBReference(deviceLabel: String, productKey: String): DatabaseReference {
        return db.getReference("UsersProducts/$userEmail/$deviceLabel/$productKey")

    }

    private fun getGroupsDBReference(deviceLabel: String, productKey: String): DatabaseReference {
        return db.getReference("Groups/$userEmail/$deviceLabel/${productKey}")
    }

    private fun getGroupDBReference(deviceLabel: String, productKey: String, groupKey:String): DatabaseReference {
        return db.getReference("Groups/$userEmail/$deviceLabel/${productKey}/$groupKey")
    }

    private fun getConsumptionDBReference(productKey: String): DatabaseReference {
        return db.getReference("Consumption/$userEmail/${productKey}")
    }
    */


    suspend fun listenForProducts(deviceLabel: String): List<Product>? {

        try {
            val call = ServiceBuilder.buildService(RestApi::class.java).getUserProducts("users/$email/devices/$deviceLabel/products")
            val products = call.body()!!.data
            if(call.isSuccessful && products != null){
                return products
            }
        } catch (e: Exception){
            return null
        }
        return null

    }

    suspend fun listenForDevices(): List<Device>? {

        try {
            var e = email
            val call = ServiceBuilder.buildService(RestApi::class.java).getUserDevices("users/$email/devices/")
            val devices = call.body()!!.data
            if(call.isSuccessful && devices != null){
                return devices
            }
        } catch (e: Exception){
            return null
        }
        return null

    }

    suspend fun listenForGroups(device: String, productKey: String): List<Group>? {

        try {
            val call = ServiceBuilder.buildService(RestApi::class.java).getUserGroups("users/$email/products/$productKey/groups")
            val groups = call.body()!!.data
            if(call.isSuccessful && groups != null){
                return groups
            }
        } catch (e: Exception){
            return null
        }
        return null
    }

    fun updateGroup(group: Group): Boolean{
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        val call = retrofit.updateGroup(group, "/api/users/$email/groups/${group.key}").execute()
        val g = call.body()
        if(call.isSuccessful && g != null){
            return true
        }
        return false
    }


    fun addUnitsToProduct(device: String, productKey:String, newCount: Int) {
        /*val reference = getProductDBReference(device, productKey)
        val listenner = reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val product: Product =  snapshot.getValue(Product::class.java)!!
                    reference.removeEventListener(this)
                    updateProductCount(device, productKey, product.remainingUnits!!.toInt() + newCount)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })*/
    }



    fun addUser(user: User, onResult: (Boolean) -> Unit) {
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        retrofit.addUser(user, "users/${user.email!!}").enqueue(
            object : Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    onResult(false)
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    val addedUser = response.body()
                    onResult(true)
                }
            }
        )
    }

    fun addProduct(item: Product, email: String): Product? {
        var r = Product()
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        println("path: users/$email/devices/${item.deviceKey}/products")
        println("product: $item")
        val call = retrofit.addProduct(item, "users/$email/devices/${item.deviceKey}/products").execute()
        val g = call.body()

        if(call.isSuccessful && g != null) {
            return g
        }
        return null
    }

    fun addGroup(group: Group): Group? {
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        val call = retrofit.addGroup(group, "users/$email/products/${group.productKey}/groups").execute()
        if (call.isSuccessful && call.body() != null){
            return call.body()
        }
        return null
    }

    fun addDevice(device: Device, email:String): Device? {
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        val call = retrofit.addDevice(device, "users/${email}/devices").execute()

        val deviceResponse = call.body()
        if(call.isSuccessful && deviceResponse != null){
            return deviceResponse
        }

        return null
    }

    fun deleteGroup(group: Group): Boolean {
        group.productKey = "null"
        return updateGroup(group)
    }

    fun deleteProduct(product: Product): Boolean {
        product.deviceKey = "null"
        return updateProduct(product)
    }

    fun updateProduct(group: Product): Boolean{
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        val call = retrofit.updateProduct(group, "/api/users/$email/products/${group.key}").execute()
        val g = call.body()
        if(call.isSuccessful && g != null){
            return true
        }
        return false
    }
}
