package com.app.freasy.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.models.Device


class DeviceListAdapter(var devices: List<Device>): RecyclerView.Adapter<DeviceListAdapter.item>() {

    inner class item(itemView: View, listener: OnCardListener): RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var description: TextView

        init {
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)

            itemView.setOnClickListener{
                listener.onCardClick(adapterPosition)
            }

        }
    }

    fun clear() {
        devices = listOf<Device>()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): item {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_item, viewGroup, false)
        return item(v, listener)
    }

    override fun onBindViewHolder(viewHolder: item, i: Int) {
        viewHolder.name.text = devices[i].label
        viewHolder.description.text = devices[i].brand
    }

    override fun getItemCount(): Int {
        return devices.size
    }

    private lateinit var listener: OnCardListener

    fun setOnClickListener(mlistener: OnCardListener){
        listener = mlistener
    }

    public interface OnCardListener{
        fun onCardClick(position: Int)
    }
}