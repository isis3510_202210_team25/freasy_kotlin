package com.app.freasy.views

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.GlobalApp
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.models.Product
import com.app.freasy.util.ProductListAdapter
import java.time.LocalDate
import com.app.freasy.GlobalApp.Companion.preferences
import com.app.freasy.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter

class PreferedFoods : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private var products = listOf<Product>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rotten_items)
        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewExp)
        var title: TextView = findViewById(R.id.tittleExp)
        title.text = "Prefered products"
        recyclerView.layoutManager = LinearLayoutManager(this)
        val backButton: ImageButton = findViewById(R.id.backButtonCupExp)
        backButton.setOnClickListener {
            val intent = Intent(this, StatsActivity::class.java)
            startActivity(intent)
        }

        CoroutineScope(Dispatchers.Main).launch {
            val devices = GlobalApp.cManager.listenForDevices()
            for (device in devices) {
                val products = GlobalApp.cManager.listenForProducts(device.key!!)
                var max = 0
                var maxProduct = null
                for (product in products){
                    for (group in cManager.listenForGroups(device.key!!, product.key!!)!!){

                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun getProducts(_products: List<Product>?, key: String) {
        val current = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val currentDate = LocalDate.parse(current.toString(), formatter)
        var s: String
        var y: Int = 0
        var m: Int = 0
        var d: Int = 0
        var f: List<String?> = List<String?>(3){null; null; null}
        var date: LocalDate
        if (_products != null) {
            for (p in _products) {
                var groups = GlobalApp.cManager.listenForGroups(key, p.key!!)
                for (g in groups!!) {
                    s = g.expiresAt!!
                    f = s?.split("/")
                    y = f!![2].toInt()
                    m = f!![1].toInt()
                    d = f!![0].toInt()
                    date = LocalDate.of(y, m, d)
                    var comp = date.compareTo(currentDate)
                    if (comp < 0) {
                        products += p
                        val adapter = ProductListAdapter(products)
                        recyclerView.adapter = adapter
                        adapter.setOnClickListener(object : ProductListAdapter.OnCardListener {
                            override fun onCardClick(position: Int) {
                                openGroup(position)
                            }
                        })
                    }
                }
            }
        }
    }

    fun openGroup(i: Int) {
        val intent = Intent(this, GroupActivity::class.java)
        intent.putExtra("key", products[i].key);
        intent.putExtra("device", "cupboard");

        startActivity(intent)
    }
}