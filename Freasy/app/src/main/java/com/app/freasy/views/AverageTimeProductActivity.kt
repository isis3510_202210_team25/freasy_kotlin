package com.app.freasy.views

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.app.freasy.R
import com.app.freasy.models.Product
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import com.app.freasy.GlobalApp.Companion.cManager
import com.app.freasy.GlobalApp.Companion.preferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.temporal.ChronoUnit

@RequiresApi(Build.VERSION_CODES.O)
class AverageTimeProductActivity : AppCompatActivity() {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val currentDate = LocalDate.parse(LocalDate.now().toString(), formatter)
    private var products = listOf<Product>()
    private var count: Int = 0
    private var avg: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_average_time_product)
        var txt: TextView = findViewById<TextView>(R.id.textAvg)
        CoroutineScope(Dispatchers.Default).launch {

            val devices = cManager.listenForDevices()
            var total:Long = 0
            for (device in devices){
                val productsF = cManager.listenForProducts(device.key!!)
                total += getProducts(productsF, device.key!!)
            }
            
            println("$total")
            if (count != 0) {
                avg = (total / count)
            }
            txt.text = "$avg"

        }
    }

    private suspend fun getProducts(_products: List<Product>?, key: String): Long {
        var temp: Long = 0
        if (_products != null) {
            for (p in _products) {
                val groups = cManager.listenForGroups(key, p.key!!)
                for (g in groups!!) {
                    val s: String? = g.expiresAt
                    var g = s?.split("/")
                    var y = g!![2].toInt()
                    var m = g!![1].toInt()
                    var d = g!![0].toInt()
                    var date = LocalDate.of(y, m, d)
                    var comp = date.compareTo(currentDate)
                    if (comp > 0) {
                        products += p
                        println("$date")
                        println("$currentDate")
                        var resp: Long = ChronoUnit.DAYS.between(currentDate, date)
                        println("$resp")
                        temp += resp
                        count++
                    }
                }
            }
        }
        return temp
    }

}