package com.app.freasy.models

import kotlinx.serialization.Serializable

@Serializable
data class Consumption(
    var userKey: String? = "",
    var deviceKey: String? = "",
    var productKey: String? = "",
    var groupKey: String? = "",
    var key: String? = "",
    var at: String? = "",
    var units: Int? = 0
)
