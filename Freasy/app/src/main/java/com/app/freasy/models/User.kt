package com.app.freasy.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("key") var key: String? = "",
    @SerializedName("birth") var birth: String? = "",
    @SerializedName("name") var name: String? = "",
    @SerializedName("email") var email: String? = "",
    @SerializedName("password") var password: String? = ""
)
