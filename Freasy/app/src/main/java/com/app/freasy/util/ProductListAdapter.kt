package com.app.freasy.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.models.Product


class ProductListAdapter(var products: List<Product>): RecyclerView.Adapter<ProductListAdapter.item>() {

    inner class item(itemView: View, listener: OnCardListener): RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var description: TextView

        init {
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)

            itemView.setOnClickListener{
                listener.onCardClick(adapterPosition)
            }

        }
    }

    fun clear() {
        products = listOf<Product>()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): item {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_item, viewGroup, false)
        return item(v, listener)
    }

    override fun onBindViewHolder(viewHolder: item, i: Int) {
        viewHolder.name.text = products[i].name
        viewHolder.description.text = products[i].description
    }

    override fun getItemCount(): Int {
        return products.size
    }

    private lateinit var listener: OnCardListener

    fun setOnClickListener(mlistener: OnCardListener){
        listener = mlistener
    }

    public interface OnCardListener{
        fun onCardClick(position: Int)
    }
}