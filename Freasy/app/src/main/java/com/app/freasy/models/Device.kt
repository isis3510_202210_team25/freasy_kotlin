package com.app.freasy.models

import kotlinx.serialization.Serializable

@Serializable
data class Device(
    var userKey: String? = "",
    var key: String? = "",
    var label: String? = "",
    var brand: String? = ""
)
