package com.app.freasy.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.*

@Serializable
data class Product(
@SerializedName("image")
@Expose
var image: String? = "",
@SerializedName("deviceKey")
@Expose
var deviceKey: String? = "",
@SerializedName("calories")
@Expose
var calories: Int? = 0,
@SerializedName("name")
@Expose
var name: String? = "",
@SerializedName("description")
@Expose
var description: String? = "",
@SerializedName("key")
@Expose
var key: String? = ""
)
