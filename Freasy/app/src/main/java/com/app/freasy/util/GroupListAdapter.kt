package com.app.freasy.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.freasy.R
import com.app.freasy.models.Group

class GroupListAdapter(var groups: List<Group>): RecyclerView.Adapter<GroupListAdapter.item>() {

    inner class item(itemView: View, listener: OnCardListener): RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var description: TextView

        init {
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)

            itemView.setOnClickListener{
                listener.onCardClick(adapterPosition)
            }

        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): item {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_item, viewGroup, false)
        return item(v, listener)
    }

    override fun onBindViewHolder(viewHolder: item, i: Int) {
        viewHolder.name.text = "Units left: ${groups[i].remainingUnits}"
        viewHolder.description.text = "Shopped at: ${groups[i].addedAt}"
    }

    override fun getItemCount(): Int {
        return groups.size
    }

    private lateinit var listener: OnCardListener

    fun setOnClickListener(mlistener: OnCardListener){
        listener = mlistener
    }

    public interface OnCardListener{
        fun onCardClick(position: Int)
    }
}