package com.app.freasy.views

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.text.Editable
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import com.app.freasy.*
import com.app.freasy.models.Group
import com.app.freasy.util.DatePickerFragment
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import  com.app.freasy.GlobalApp.Companion.cManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AddGroupItem : AppCompatActivity() {

    lateinit var activityResultLauncher: ActivityResultLauncher<Intent>

    private lateinit var productName: String
    private lateinit var productKey: String
    private lateinit var device: String


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_group)

        val extras = intent.extras
        if (extras != null) {
            productName = extras.getString("productName").toString()
            productKey = extras.getString("productKey").toString()
            device = extras.getString("device").toString()
        }


        val editName: EditText = findViewById(R.id.deviceText)

        editName.isEnabled = false
        editName.setText(productName)

        val editQuantity: EditText = findViewById(R.id.brandText)
        val editDate: EditText = findViewById(R.id.date_bought)
        val editExpDate: EditText = findViewById(R.id.date_expires)
        val submitButton: Button = findViewById(R.id.addButton)
        val backButton: ImageButton = findViewById(R.id.back_button)

        backButton.setOnClickListener {
            val intent = Intent(this, DeviceActivity::class.java)
            startActivity(intent)
        }

        submitButton.setOnClickListener{
            if(verifyFields(editName, editQuantity, editDate, editExpDate)) {
                val group = Group(productKey, "", editDate.text.toString(), editExpDate.text.toString(), editQuantity.text.toString().toInt())
                CoroutineScope(Dispatchers.Main).launch{
                    val res = cManager.addGroup(group)
                    if (res != null) {
                        goHome()
                    }else {
                        showAlert("There's have been an error, please try again later.")
                    }
                }
            }
        }


        editDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDatePickerDialog(editDate)
            }
        }

        editDate.setOnClickListener {
            showDatePickerDialog(editDate)
        }

        editExpDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDatePickerDialog(editExpDate)
            }
        }

        editExpDate.setOnClickListener {
            showDatePickerDialog(editExpDate)
        }

    }

    private fun showAlert(o:String){
        Toast.makeText(this, o, Toast.LENGTH_SHORT).show()

    }

    private fun showDatePickerDialog(text: EditText) {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year, text) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int, text: EditText) {
        val monthI = month + 1
        var dayS = ""+day
        if(day < 10){
            dayS = "0$dayS"
        }
        var monthS = ""+monthI
        if(month < 10){
            monthS = "0$monthS"
        }
        text.setText("$dayS/$monthS/$year")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun verifyFields(name: EditText, quantity: EditText, shopDate: EditText, exDate: EditText): Boolean {
        if (name.text.toString().isEmpty()) {
            name.error = "Please enter a name to continue"
            name.requestFocus()
            return false
        }

        try {
            val parsedInt = quantity.text.toString().toInt()

            if (parsedInt<1) {
                name.error = "Please enter a valid quantity to continue"
                name.requestFocus()
                return false
            }

        } catch (nfe: NumberFormatException) {
            quantity.error = "Please enter a valid quantity to continue"
            quantity.requestFocus()
            return false
        }

        if (shopDate.text.toString().isEmpty()) {
            shopDate.error = "Please choose a date"
            shopDate.requestFocus()
            return false
        }

        if (exDate.text.toString().isEmpty()) {
            exDate.error = "Please choose a date"
            exDate.requestFocus()
            return false
        }

        val shopDateValue =
            LocalDate.parse(shopDate.text.toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDay =
            LocalDate.parse(sdf.format(Date()), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

        if(shopDateValue.dayOfYear < currentDay.dayOfYear || shopDateValue.year < currentDay.year){

            val d = shopDateValue.dayOfYear
            val e =  currentDay.dayOfYear
            Toast.makeText(this, "Please choose a valid shopping date $d $e", Toast.LENGTH_SHORT).show()
            shopDate.requestFocus()
            return false
        }


        val exDateValue =
            LocalDate.parse(exDate.text.toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))

        if(shopDateValue.dayOfYear > exDateValue.dayOfYear || shopDateValue.year > exDateValue.year || exDateValue.dayOfYear < currentDay.dayOfYear || exDateValue.year < currentDay.year){

            Toast.makeText(this, "Please choose a valid expiration date", Toast.LENGTH_SHORT).show()
            exDate.requestFocus()
            return false
        }
        return true
    }

    private fun goHome(){
        val homeIntent = Intent(this, DeviceList::class.java)
        startActivity(homeIntent)
    }
}
