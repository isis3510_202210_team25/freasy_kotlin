package com.app.freasy.models

import com.google.gson.annotations.SerializedName

data class DeviceResponse(
    @SerializedName("data") var data: List<Device>? = null,
)
